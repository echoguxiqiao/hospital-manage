import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Antd from '@/plugins/antd' // 引入antd组件库
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import SvgIcon from '@/components/SvgIcon' // 引入svg组件
import api from '@/apis/index.js' // 引入api文件
import '@/assets/scss/index.scss' // 引入全局样式

const app = createApp(App)
// Antd(app)
app.use(Antd)
app.use(ElementPlus)
app.use(store).use(router)
app.config.globalProperties.$api = api // 挂载接口api
app.component('svg-icon', SvgIcon) // 注册svg公共组件
app.mount('#app')
