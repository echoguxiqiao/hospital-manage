/*
 * @Author: hmw
 * @Date: 2024-3-27 18:37:04
 * @LastEditors: hmw
 * @LastEditTime:2024-3-27 19:27:37
 * @Description: 登录
 */
import request from '@/http/request.js'

// 激活用户
const activeUser = data => {
  return request.post('/user/active', data)
}

export default {
  activeUser
}
