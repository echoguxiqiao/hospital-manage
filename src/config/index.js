/*
 * @Author: Ygm
 * @Date: 2023-08-29 09:41:30
 * @LastEditors: Ygm
 * @LastEditTime: 2023-11-30 18:45:06
 * @Description: 环境基础配置文件
 */
const config = require('./env.' + process.env.NODE_ENV)
module.exports = config
