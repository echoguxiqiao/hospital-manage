/*
 * @Author: Ygm
 * @Date: 2023-11-30 13:14:29
 * @LastEditors: Ygm
 * @LastEditTime: 2023-11-30 20:08:36
 * @Description: axios请求封装
 */
import axios from 'axios'
import config from '@/config'
import { AxiosRetry } from './retry'
import { showMessage } from './status'
import { message } from 'ant-design-vue'
import { readSion, saveLocal, saveSion } from '@/utils/storage'

// 公共请求头
const BASE_PUBLIC_HEADERS = {
  'client-type': 'pc', // 平台类型
  'reqSource': 'api', // 来源
  'Content-Type': 'application/json' // 传参方式
}

// 公共请求参数
// const BASE_PUBLIC_DATA = {
//   alias: 'pc',
//   version: '3.0'
// }

// axios基础配置
const service = axios.create({
  withCredentials: false,
  baseURL: config.BASE_URL,
  timeout: 10 * 1000 // 请求超时时间(10s)
})

// 无感刷新token
const axiosRetry = new AxiosRetry({
  baseUrl: config.BASE_URL,
  url: '/api/base/auth/tokenrefresh',
  unauthorizedCode: 401,
  getRefreshToken: () => readSion('TOKEN_OBJ')?.token, // 获取刷新token
  onSuccess: res => {
    const TOKEN_OBJ = {
      token: res?.data.token,
      tokenExpire: res?.data?.tokenExpire
    }
    const ACC_TOKEN_OBJ = {
      accessToken: res?.data.accessToken,
      accessTokenExpire: res?.data?.accessTokenExpire
    }
    saveSion('TOKEN_OBJ', TOKEN_OBJ)
    saveLocal('ACC_TOKEN_OBJ', ACC_TOKEN_OBJ)
  },
  onError: () => {
    console.log('refreshToken过期了，跳转登录页')
  }
})

// 1.请求拦截器
service.interceptors.request.use(
  config => {
    const TOKEN = readSion('TOKEN_OBJ')?.token
    // 1.1 配置请求头
    config.headers = {
      token: TOKEN,
      ...BASE_PUBLIC_HEADERS,
      ...config.headers
    }
    // 1.2 请求参数配置
    config.data = {
      ...config.data 
    }
    return config
  },
  error => {
    console.log(error)
    return Promise.reject(error)
  }
)

// 2.响应拦截器
service.interceptors.response.use(
  response => {
    const res = response.data
    // res.header('Access-Control-Allow-Origin', '*');
    if (res.result && !res.result) {
      res.message = res.message ? res.message : '服务异常'
      return Promise.reject(res)
    } else if (res.code === 900) {
      res.result = true
      saveSion('MAINTAIN_INFO', response?.data)
      window.open(window.location.origin + '/#/base/maintain', '_self')
    }
    return Promise.resolve(res)
  },
  error => {
    const { response } = error
    if (response) {
      if (response.status === 401) {
        console.log('token失效 :>> ', response?.data)
      } else {
        message?.warning(showMessage(response.status))
      }
    } else {
      message?.warning('网络连接异常,请稍后再试')
    }
    return Promise.resolve(response?.data || response)
  }
)

export default {
  get (url, data,responseType,options = {}) {
    options.url = url;
    options.params = data;
    options.method = 'GET';
    options.responseType = responseType;
    return axiosRetry.requestWrapper(() => service(options));
  },
  post (url, data, options = {}) {
    options.url = url
    options.data = data
    options.method = 'POST'
    return axiosRetry.requestWrapper(() => service(options))
  },
  delete (url, data, options = {}) {
    options.url = url
    options.data = data
    options.method = 'DELETE'
    options.headers = {
      'Access-Control-Allow-Methods':'DELETE'
    }
    return axiosRetry.requestWrapper(() => service(options))
  },
  put(url, data, options = {}) {
    options.url = url
    options.data = data
    options.method = 'PUT'
    return axiosRetry.requestWrapper(() => service(options))
  }
}
