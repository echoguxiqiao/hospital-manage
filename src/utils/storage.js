/*
 * @Author: Ygm
 * @Date: 2023-11-30 13:38:56
 * @LastEditors: Ygm
 * @LastEditTime: 2023-11-30 19:12:26
 * @Description: 本地存储
 */
const prefix = 'base_pc' // 前缀

/**
 * @description: 存到session
 * @param {String} name 键名
 * @param {any} value 键值
 * @return {*}
 */
const saveSion = function (name, value) {
    if (typeof name == 'string' && name) {
        sessionStorage.setItem(prefix + '_' + name, JSON.stringify(value))
    } else {
        console.log('session命名格式有误')
    }
}

/**
 * @description: 获取session数据
 * @param {String} name 键名
 * @return {*}
 */
const readSion = function (name) {
    if (typeof name == 'string' && name) {
        let readName = sessionStorage.getItem(prefix + '_' + name)
        return readName !== 'undefined' ? JSON.parse(readName) : null
    } else {
        console.log('查询字段有误')
    }
}

/**
 * @description: 移除session数据
 * @param {String} name 键名
 * @return {*}
 */
const removeSion = function (name) {
    if (typeof name == 'string' && name) {
        sessionStorage.removeItem(prefix + '_' + name)
    } else {
        console.log('查询字段有误')
    }
}

/**
 * @description: 加前缀localStorage封装
 * @param {String} name 键名
 * @param {any} value 键值
 * @return {*}
 */
const saveLocal = function (name, value) {
    if (typeof name == 'string' && name) {
        localStorage.setItem(prefix + '_' + name, JSON.stringify(value))
    } else {
        console.log('命名格式有误')
    }
}

/**
 * @description: 获取localStorage数据
 * @param {String} name 键名
 * @return {*}
 */
const readLocal = function (name) {
    if (typeof name == 'string' && name) {
        let readName = localStorage.getItem(prefix + '_' + name)
        return readName !== 'undefined' ? JSON.parse(readName) : null
    } else {
        console.log('查询字段有误')
    }
}

/**
 * @description: 移除session数据
 * @param {String} name 键名
 * @return {*}
 */
const removeLocal = function (name) {
    if (typeof name == 'string' && name) {
        localStorage.removeItem(prefix + '_' + name)
    } else {
        console.log('查询字段有误')
    }
}

/**
 * @description 设置cookie
 * @param name name cookie名
 * @param value value cookie值
 * @param time time cookie过期时间以秒为单位
 * @param path path cookie的路径
 * @return
 */
const setCookie = (name, value, time = '', path = '') => {
    let exp
    let strsec
    if (time && path) {
        strsec = time * 1000
        exp = new Date()
        exp.setTime(exp.getTime() + strsec)
        document.cookie =
            name + '=' + escape(value) + ';expires=' + exp.toGMTString() + ';path=' + path
    } else if (time) {
        strsec = time * 1000
        exp = new Date()
        exp.setTime(exp.getTime() + strsec)
        document.cookie = name + '=' + escape(value) + ';expires=' + exp.toGMTString()
    } else if (path) {
        document.cookie = name + '=' + escape(value) + ';path=' + path
    } else {
        document.cookie = name + '=' + escape(value)
    }
}

/**
 * @description 获取cookie
 * @param name name of the cookie
 * @returns {null|string}
 */
const getCookie = name => {
    let arr,
        reg = new RegExp('(^| )' + name + '=([^;]*)(;|$)')
    if ((arr = document.cookie.match(reg))) {
        return unescape(arr[2])
    } else {
        return null
    }
}

/**
 * @description 删除cookie
 * @param name 删除cookie
 * @returns
 */
const removeCookie = name => {
    const exp = new Date()
    exp.setTime(exp.getTime() - 1)
    // 这里需要判断一下cookie是否存在
    const c = getCookie(name)
    if (c != null) {
        document.cookie = name + '=' + c + ';expires=' + exp.toGMTString()
    }
}

export {
    saveSion,
    readSion,
    removeSion,
    saveLocal,
    readLocal,
    removeLocal,
    setCookie,
    getCookie,
    removeCookie
}
