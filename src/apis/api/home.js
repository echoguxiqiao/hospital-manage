/*
 * @Author: hmw
 * @Date: 2024-3-29 14:43:04
 * @LastEditors: hmw
 * @Description: 首页
 */

import request from '@/http/request.js'

// 激活用户
const getMenuList = data => {
    return request.get('/menu/getMenuTree', data)
}

  
  export default {
    getMenuList
  }