/*
 * @Author: hmw
 * @Date: 2024-3-27 13:08:43
 * @LastEditors: hmw
 * @LastEditTime: 2024-3-27 13:11:00
 * @Description: api入口文件
 */
let apis = {}
const context = require.context('./api', true, /\.js$/)
context.keys().forEach((e) => {
    apis = {
        ...apis,
        ...context(e).default,
    }
})

export default apis
