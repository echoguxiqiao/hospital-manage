/*
 * @Author: Ygm
 * @Date: 2023-11-30 14:51:06
 * @LastEditors: Ygm
 * @LastEditTime: 2023-11-30 16:24:10
 * @Description: 全局路由守卫
 */
import { readSion } from '@/utils/storage'


// 路由前置守卫
function createBeforeEachGuard (router) {
  router.beforeEach((to, from, next) => {
    next();
  })
}

// 路由后置守卫
// function createAfterEachGuard (router) {
//   router.afterEach(to => {
//     // const hasToken = readSion('TOKEN_OBJ')?.token
//     const hasInitInfo = !!readSion('INIT_ACCOUNT')
//     if (to?.meta?.ignoreAuth) return
//     if (!hasToken && to.name !== 'Login') {
//       // 如果没有登录且目标路由不是登录页,则跳转到登录页；如果有激活信息，则跳到激活页
//       router.push({ name: !hasInitInfo ? 'Login' : 'InitAccount', params: { ...to.query } })
//     } else if (hasToken && to.name !== 'Home') {
//       // 如果已经登录且目标页面不是首页，则跳转至首页
//       router.push({ name: 'Home', params: { ...to.query } })
//     }
//   })
// }

export function createRouterGuard (router) {
  createBeforeEachGuard(router)
  // createAfterEachGuard(router)
}
