/*
 * @Author: Ygm
 * @Date: 2023-11-30 11:47:14
 * @LastEditors: Ygm
 * @LastEditTime: 2023-11-30 18:11:08
 * @Description: 路由配置
 */

import { createRouter, createWebHashHistory } from 'vue-router'
import { createRouterGuard } from '@/router/guard'
import Layout from "@/layout/index.vue";

// 动态路由数组
const dynamicRoutes = [];

const routes = [
    {
        path: '/',
        redirect: '/login'
    },
    {
        path: '/login',
        name: 'Login',
        meta: {
            title: '凉山彝族自治州第二人民医院后台管理系统',
            keepAlive: false
        },
        component: () => import('@/views/Login')
    },
    {
        path: '/',
        name: 'Layout',
        component: Layout,
        // redirect: '/index',
        children: [
          {
            path: '/user',
            name: '用户管理',
            meta: {
                title: '用户管理',
                keepAlive: false 
            },
            component: () => import('@/views/Home/User')
          },
          {
            path: '/role',
            name: '角色管理',
            meta: {
                title: '角色管理',
                keepAlive: false 
            },
            component: () => import('@/views/Home/Role')
          },
          {
            path: '/menu',
            name: '菜单管理',
            meta: {
                title: '菜单管理',
                keepAlive: false 
            },
            component: () => import('@/views/Home/Menu')
          },
          {
            path: '/tag',
            name: '标签管理',
            meta: {
                title: '标签管理',
                keepAlive: false
            },
            component: () => import('@/views/Home/Tag')
          },
          {
            path: '/contentManage',
            name: '内容管理',
            meta: {
                title: '内容管理',
                keepAlive: false
            },
            component: () => import('@/views/Home/ContentManage')
          }
        ]
    }
]

// 实例化路由
const router = createRouter({
    history: createWebHashHistory(),
    routes
})

createRouterGuard(router)

export default router
