/*
 * @Author: Ygm
 * @Date: 2023-11-30 16:11:41
 * @LastEditors: Ygm
 * @LastEditTime: 2023-11-30 18:31:41
 * @Description: 登录
 * 
 */
//这个文件没生效
import { readLocal, readSion, saveLocal, saveSion } from '@/utils/storage'

export default {
  namespaced: true, // 开启强制命名空间
  state: () => ({
    tokenObj: readSion('TOKEN_OBJ') || null,
    accessTokenObj: readLocal('ACC_TOKEN_OBJ') || null,
    userInfo: readSion('USER_INFO') || null,
    initAccount: readSion('INIT_ACCOUNT') || null
  }),
  mutations: {
    // 存储token信息
    setTokenObj (state, data) {
      state.tokenObj = data
      saveSion('TOKEN_OBJ', data)
    },

    // 存储accessToken
    setAccessTokenObj (state, data) {
      state.accessTokenObj = data
      saveLocal('ACC_TOKEN_OBJ', data)
    },

    // 存储用户信息
    setUserInfo (state, data) {
      state.userInfo = data
      saveSion('USER_INFO', data)
    },

    // 存储激活信息
    setInitAccount (state, data) {
      state.initAccount = data
      saveSion('INIT_ACCOUNT', data)
    }
  },
  actions: {}
}
