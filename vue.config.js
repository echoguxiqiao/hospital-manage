const { defineConfig } = require('@vue/cli-service')
const path = require('path')
const resolve = dir => path.join(__dirname, dir)

module.exports = defineConfig({
    publicPath: './', // 署应用包时的基本URL
    outputDir: 'dist', //  生产环境构建文件的目录
    assetsDir: 'static', //  outputDir的静态资源(js、css、img、fonts)目录
    lintOnSave: false, // eslint 检测
    transpileDependencies: true,
    productionSourceMap: false, // 生产环境的sourcemap
    // css相关配置
    css: {
        extract: { ignoreOrder: true },
        loaderOptions: {
            less: {
                lessOptions: {
                    javascriptEnabled: true
                }
            },
            postcss: {
                postcssOptions: {
                    plugins: {
                        autoprefixer: {}, // 用来给不同的浏览器自动添加相应前缀，如-webkit-，-moz-等等
                        'postcss-px-to-viewport': {
                            unitToConvert: 'px',
                            viewportWidth: 1920,
                            viewportHeight: 1080,
                            unitPrecision: 3,
                            propList: ['*'],
                            viewportUnit: 'vw', //指定需要转换成的视窗单位，建议使用vw
                            fontViewportUnit: 'vw',
                            selectorBlackList: [], // 指定不转换为视窗单位的类
                            minPixelValue: 10, // 小于或等于`10px`不转换为视窗单位
                            mediaQuery: false, // 允许在媒体查询中转换`px`
                            replace: true,
                            exclude: [], // 设置忽略文件，用正则做目录名匹配
                            // include: /\/node_modules\/\//, // 只有匹配到的文件才会被转换
                            landscape: false, // 是否自动加@media
                            landscapeUnit: 'vw', // 横屏单位
                            landscapeWidth: 1334 // 横屏宽度
                        }
                    }
                }
            }
        }
    },
    devServer: {
        port: 8089, // 端口
        open: false, // 启动后打开浏览器
        client: {
            overlay: false
        },
        proxy: {
            // 配置跨域
            '/api': {
                // target: 'http://192.168.2.171:8999',
                // target: 'http://192.168.0.41:8999',
                target: 'http://47.120.42.70:8999',
                changOrigin: true
                // pathRewrite: (path) => path.replace(/^\/api/, ""), // replace /api
            }
        }
    },

    chainWebpack(config) {
        config.plugins.delete('preload') // TODO: need test
        config.plugins.delete('prefetch') // TODO: need test

        // set svg-sprite-loader
        config.module.rule('svg').exclude.add(resolve('src/assets/icons')).end()
        config.module
            .rule('icons')
            .test(/\.svg$/)
            .include.add(resolve('src/assets/icons'))
            .end()
            .use('svg-sprite-loader')
            .loader('svg-sprite-loader')
            .options({
                symbolId: 'icon-[name]'
            })
            .end()
    }
})
