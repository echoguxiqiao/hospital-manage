/*
 * @Author: hmw
 * @Date: 2024-3-27 18:37:04
 * @LastEditors: hmw
 * @LastEditTime: 2024-3-27 19:27:37
 * @Description: 登录
 */
import request from '@/http/request.js'

// 获取图片验证码
const getVerifyimg = data => {
  return request.get('/user/refreshCode', data,'blob')
}

// 用户登录
const userLogin = data => {
  return request.post( '/login', data);
}

// 获取用户信息
const getUserInfo = data => {
  return request.post( '/auth/tokenverify', data)
}


//tag页面

//表格内容
const getTagList = data => {
  return request.get('/tag/get', data)
}

//删除单行
const deletTagRow = data => {
  return request.delete(`/tag/delete/${data.id}`)
}

//添加标签
const addTag = data => {
  return request.post('/tag/add', data)  
}

//修改标签
const modifyTag = data => {
    return request.put('/tag/update', data)  
  }
  
//内容管理页

//获取内容列表
const getContentList = data => {
  return request.get('/web/content/select', data)
}

export default {
  getVerifyimg,
  userLogin,
  getUserInfo,
  getTagList,
  deletTagRow,
  addTag,
  modifyTag,
  getContentList
}
