/*
 * @Author: hmw
 * @Date: 2024-4-01 14:43:04
 * @LastEditors: hmw
 * @Description: 标签管理
 */

import request from '@/http/request.js'

//二级菜单内容
const getTagList = data => {
  return request.get('/tag/get', data)
}

//删除单行
const deletTagRow = data => {
  return request.delete(  `/tag/delete/${data.ids}`)
}

//添加标签
const addTag = data => {
  return request.post('/tag/add', data)  
}

//修改标签
const modifyTag = data => {
    return request.put('/tag/update', data)  
  }  
export default {
    getTagList,
    deletTagRow,
    addTag,
    modifyTag
  }