/*
 * @Author: Ygm
 * @Date: 2023-03-29 09:56:58
 * @LastEditors: Ygm
 * @LastEditTime: 2023-11-30 13:18:46
 * @Description: 正式环境配置
 */
module.exports = {
    title: '正式环境',
    BASE_URL: window.location.origin // 线上环境接口地址
}
