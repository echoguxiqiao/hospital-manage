import {
    Button,
    Checkbox,
    DatePicker,
    Form,
    Input,
    message,
    Modal,
    Card,
    Space,
    Menu,
    Popover,
    Dropdown
} from 'ant-design-vue'

message.config({
    top: `100px`,
    duration: 2,
    maxCount: 1,
    getContainer: () => {
        return document.getElementById('showBigModal') || document.body
    }
})

const components = [
    Button,
    Checkbox,
    DatePicker,
    Form,
    Input,
    message,
    Modal,
    Card,
    Space,
    Menu,
    Popover,
    Dropdown
]

export default function Antd(app) {
    components.forEach(component => {
        app.use(component)
    })
    app.config.globalProperties.$message = message
    app.config.globalProperties.$confirm = Modal.confirm
}
