/*
 * @Author: Ygm
 * @Date: 2023-11-30 13:20:46
 * @LastEditors: Ygm
 * @LastEditTime: 2023-11-30 14:26:19
 * @Description: 通用工具集
 */
/**
 * @description: 从URL中获取指定参数
 * @param {String} queryName 参数名
 * @return {String}
 */
function getQueryValue (queryName) {
  const reg = new RegExp('(^|&)' + queryName + '=([^&]*)(&|$)', 'i')
  const r = window.location.search.substring(1).match(reg)
  return r != null ? decodeURI(r[2]) : null
}

/**
 * @description: 获取Url参数，返回一个对象。例:?a=1&b=2&c=3 ==> {a: "1", b: "2", c: "3"}
 * @return {Object}
 */
const getUrlParams = () => {
  let url = document.location.toString()
  let arrObj = url.split('?')
  let params = Object.create(null)
  if (arrObj.length > 1) {
    arrObj = arrObj[1].split('&')
    arrObj.forEach(item => {
      item = item.split('=')
      params[item[0]] = item[1]
    })
  }
  return params
}

/**
 * @description: 把字节转换成正常文件大小
 * @param {Number} size 文件大小（单位：字节）
 * @return {String}
 */
const getFileSize = size => {
  if (!size) return '0KB'
  var num = 1024.0 // byte
  if (size < num) return size + 'B'
  if (size < Math.pow(num, 2)) return (size / num).toFixed(2) + 'KB' // kb
  if (size < Math.pow(num, 3)) return (size / Math.pow(num, 2)).toFixed(2) + 'MB' // M
  if (size < Math.pow(num, 4)) return (size / Math.pow(num, 3)).toFixed(2) + 'G' // G
  return (size / Math.pow(num, 4)).toFixed(2) + 'T' // T
}

/**
 * @description: 生成uuid
 * @param {*} len 长度
 * @param {*} radix 基数
 * @return {*}
 */
const UUID = (len, radix) => {
  let i
  let uuid = []
  let chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split('')
  radix = radix || chars.length
  if (len) {
    for (i = 0; i < len; i++) uuid[i] = chars[0 | (Math.random() * radix)]
  } else {
    let r
    uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-'
    uuid[14] = '4'
    for (i = 0; i < 36; i++) {
      if (!uuid[i]) {
        r = 0 | (Math.random() * 16)
        uuid[i] = chars[i == 19 ? (r & 0x3) | 0x8 : r]
      }
    }
  }
  return uuid.join('')
}

/**
 * @description 项目本地assets资源得本地图片
 * @param {string} url 图片地址
 * @returns {string}
 */
async function imgToBase64 (url) {
  return new Promise((resolve, reject) => {
    const image = new Image()
    image.src = url
    image.onload = () => {
      const canvas = document.createElement('canvas')
      canvas.width = image.naturalWidth // 使用 naturalWidth 为了保证图片的清晰度
      canvas.height = image.naturalHeight
      canvas.style.width = `${canvas.width / window.devicePixelRatio}px`
      canvas.style.height = `${canvas.height / window.devicePixelRatio}px`
      const ctx = canvas.getContext('2d')
      if (!ctx) {
        return null
      }
      ctx.drawImage(image, 0, 0)
      const base64 = canvas.toDataURL('image/png')
      return resolve(base64)
    }
    image.onerror = (err) => {
      return reject(err)
    }
  })
}

export default {
  getQueryValue,
  getUrlParams,
  getFileSize,
  UUID,
  imgToBase64
}
