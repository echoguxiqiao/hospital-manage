import { createStore } from 'vuex'
import login from './modules/login'

export default createStore({
    state: { 
      menuData: []},
    getters: {},
    mutations: {
			setMenu(state, menuData) {
					state.menuData = menuData;
					}
    },
    actions: {
			saveMenu({ commit }, menuData) {
					commit('setMenu', menuData);
				}
    },
    modules: {
        login
    }
})
