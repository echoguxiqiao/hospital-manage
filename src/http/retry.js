/*
 * @Author: Ygm
 * @Date: 2023-11-30 18:20:47
 * @LastEditors: Ygm
 * @LastEditTime: 2023-11-30 19:26:29
 * @Description: Token无感知刷新
 */
import { Axios } from 'axios'
import { readLocal } from '@/utils/storage'

export class AxiosRetry {
  // 维护一个promise
  fetchNewTokenPromise = null

  constructor ({ baseUrl, url, getRefreshToken, unauthorizedCode = 401, onSuccess, onError }) {
    this.baseUrl = baseUrl
    this.url = url
    this.getRefreshToken = getRefreshToken
    this.unauthorizedCode = unauthorizedCode
    this.onSuccess = onSuccess
    this.onError = onError
  }

  requestWrapper (request) {
    return new Promise((resolve, reject) => {
      // 先把请求函数保存下来
      const requestFn = request
      return request()
      .then(resolve)
      .catch(err => {
        const isTokenReqUrl = err?.config?.url === this.url
        const isTokenInvalid = err?.request?.status === this.unauthorizedCode
        if (isTokenInvalid && !(isTokenReqUrl)) {
          if (!this.fetchNewTokenPromise) {
            this.fetchNewTokenPromise = this.fetchNewToken()
          }
          this.fetchNewTokenPromise
          .then(() => {
            // 获取token成功后，重新执行请求
            requestFn().then(resolve).catch(reject)
          })
          .finally(() => {
            // 置空
            this.fetchNewTokenPromise = null
          })
        } else {
          reject(err)
        }
      })
    })
  }

  // 获取token的函数
  fetchNewToken () {
    return new Axios({ baseURL: this.baseUrl })
    .post(
      this.url,
      {},
      {
        headers: {
          token: this.getRefreshToken(),
          accessToken: readLocal('ACC_TOKEN_OBJ').accessToken
        }
      }
    )
    .then(this.onSuccess)
    .catch(() => {
      this.onError()
      return Promise.reject()
    })
  }
}
