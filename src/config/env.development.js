/*
 * @Author: Ygm
 * @Date: 2023-03-29 09:56:58
 * @LastEditors: Ygm
 * @LastEditTime: 2023-11-30 13:18:37
 * @Description: 本地环境配置
 */
module.exports = {
  title: '本地环境',
  BASE_URL: '/api'
}
